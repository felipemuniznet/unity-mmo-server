package com.shnok.javaserver.enums;

public enum Intention {
    INTENTION_IDLE,
    INTENTION_WAITING,
    INTENTION_MOVE_TO
}
